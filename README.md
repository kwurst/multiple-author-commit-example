# Multiple Author Commit Example

Showing a co-author in a commit message.

Useful for pair programming or when a series of commits by multiple authors have been squashed.
